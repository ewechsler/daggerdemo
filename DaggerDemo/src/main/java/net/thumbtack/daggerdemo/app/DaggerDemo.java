package net.thumbtack.daggerdemo.app;

import android.app.Application;
import dagger.ObjectGraph;

public class DaggerDemo extends Application {

    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        objectGraph = ObjectGraph.create(new DaggerDemoModule());
    }

    public void inject(Object object) {
        objectGraph.inject(object);
    }
}
