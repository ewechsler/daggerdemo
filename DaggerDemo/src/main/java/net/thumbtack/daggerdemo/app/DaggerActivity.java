package net.thumbtack.daggerdemo.app;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import net.thumbtack.daggerdemo.app.model.DemoModel;

import javax.inject.Inject;


public class DaggerActivity extends ActionBarActivity implements DemoModel.Listener {

    private TextView slideTextView;

    @Inject
    DemoModel model;

    private void displaySlide() {
        slideTextView.setText(getString(R.string.slideNumber, model.getCurrentSlide() + 1, model.getSlidesCount()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ((DaggerDemo)getApplication()).inject(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dagger);
        slideTextView = (TextView)findViewById(R.id.slideText);
        Button nextButton = (Button) findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.nextSlide();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        model.setListener(this);
        model.ensureInitialized();
    }

    @Override
    protected void onPause() {
        super.onPause();
        model.setListener(null);
    }

    @Override
    public void onChanged() {
        displaySlide();
    }
}
