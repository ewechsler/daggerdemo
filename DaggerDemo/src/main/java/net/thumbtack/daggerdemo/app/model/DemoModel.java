package net.thumbtack.daggerdemo.app.model;

import android.os.AsyncTask;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DemoModel {

    public interface Listener {
        void onChanged();
    }

    private int currentSlide = -1;
    private int slidesCount;
    private Listener listener;
    private final SlidesDataSource slidesDataSource;

    @Inject
    public DemoModel(SlidesDataSource slidesDataSource) {
        this.slidesDataSource = slidesDataSource;
    }

    public void nextSlide() {
        new AsyncTask<Integer, Void, Integer[]>() {

            @Override
            protected Integer[] doInBackground(Integer... currentSlide) {
                return slidesDataSource.nextSlideWithCount(currentSlide[0]);
            }


            @Override
            protected void onPostExecute(Integer[] nextSlide) {
                DemoModel.this.currentSlide = nextSlide[0];
                DemoModel.this.slidesCount = nextSlide[1];
                notifyListener();
            }
        }.execute(currentSlide);
    }

    public void ensureInitialized() {
        if (currentSlide >= 0) {
            notifyListener();
        }

        new AsyncTask<Void, Void, Integer>() {

            @Override
            protected Integer doInBackground(Void...params) {
                return slidesDataSource.count();
            }

            @Override
            protected void onPostExecute(Integer count) {
                DemoModel.this.currentSlide = 0;
                DemoModel.this.slidesCount = count;
                notifyListener();
            }
        }.execute();
    }

    public int getCurrentSlide() {
        return currentSlide;
    }

    public int getSlidesCount() {
        return slidesCount;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private void notifyListener() {
        if (listener != null) {
            listener.onChanged();
        }
    }
}
