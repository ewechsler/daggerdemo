package net.thumbtack.daggerdemo.app.model;

public class SlidesDataSource {
    private final static int SLIDES = 20;

    public Integer[] nextSlideWithCount(Integer integer) {
        return new Integer[]{integer + 1 < SLIDES ?
                integer + 1 : integer, SLIDES};
    }

    public Integer count() {
        return SLIDES;
    }
}
