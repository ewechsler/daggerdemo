package net.thumbtack.daggerdemo.app;


import dagger.Module;
import dagger.Provides;
import net.thumbtack.daggerdemo.app.model.DemoModel;
import net.thumbtack.daggerdemo.app.model.SlidesDataSource;

@Module(injects = {
            DaggerActivity.class,
            DemoModel.class
        }
)
public class DaggerDemoModule {

    @Provides
    SlidesDataSource providesSlidesDataStore() {
        return new SlidesDataSource();
    }
}
